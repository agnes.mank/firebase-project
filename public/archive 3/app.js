import {
    initializeApp
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";

import {
    getDatabase,
    set,
    ref,
    onValue,
    update,
    remove,
    get,
    child
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

import {
    getFirestore,
    doc,
    getDoc,
    setDoc,
    query,
    collection,
    getDocs,
    where,
    updateDoc,
    deleteDoc
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";

import {
    getAuth,
    signInWithEmailAndPassword,
    AuthErrorCodes,
    createUserWithEmailAndPassword,
    signOut,
    onAuthStateChanged,
    GoogleAuthProvider
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";


// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyA7jRfkQ3WeOu7PxOBDfyGVa2BWzN9meSU",
    authDomain: "project-arpfrontendpl1.firebaseapp.com",
    projectId: "project-arpfrontendpl1",
    storageBucket: "project-arpfrontendpl1.appspot.com",
    messagingSenderId: "1035264414402",
    appId: "1:1035264414402:web:17cebf490c83cb8f2ba15b",
    measurementId: "G-P4Q78T7H6G",
    databaseURL: "https://project-arpfrontendpl1-default-rtdb.europe-west1.firebasedatabase.app",
};

const app = initializeApp(firebaseConfig);

// 1. Inicjalizacja modułu Auth
const auth = getAuth(app);
const database = getDatabase();

// 2. Definiowanie elementów UI
const emailForm = document.querySelector("#emailForm");
const passwordForm = document.querySelector("#passwordForm");
const nameForm = document.querySelector("#nameForm");
const surnameForm = document.querySelector("#surnameForm");
const dateOfBirthForm = document.querySelector("#dateOfBirthForm");
const loginBtn = document.querySelector("#loginBtn");
const signUpBtn = document.querySelector("#signUpBtn");
const phoneForm = document.querySelector("#phoneForm");
const userData = document.querySelector("#userData");

//dodawanie obiektow do realtime
const saveToRealtime = (uid) => {
    const valueEmail = emailForm.value;
    const valueName = nameForm.value;
    const valueSurname = surnameForm.value;
    const valueDateOfBirth = dateOfBirthForm.value;
    const valuePhone = phoneForm.value; 
    console.log("valueDateOfBirth: " + valueDateOfBirth);

    set(ref(database, "users/" + uid), {
        email: valueEmail,
        name: valueName,
        surname: valueSurname,
        dateOfBirth: valueDateOfBirth,
        phone: valuePhone
    });
}

// 4. Dodanie możliwość rejestracji użytkownika
const signUpUser = async () => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;

    try {
        const user = await createUserWithEmailAndPassword(auth, valueEmail, valuePassword);
        console.log(user);

        // Dodanie danych do bazy danych (np. Firestore),
        // ale u nas w celach ćwieczniowych wykorzystamy Realtime Database
        const userUid = user.user.uid;
        saveToRealtime(userUid);

    } catch (error) {
        console.warn(error);
    }
};

signUpBtn.addEventListener('click', signUpUser);

const getFromRealtime = (uid) => {
    // Pobieranie jednego konkretnego elementu
    /*const userRef = ref(database, "users/" + uid);
    onValue(userRef, (snapshot) => {
    const userSnapshot = snapshot.val();
    let userInfo = `${userSnapshot.email} ${userSnapshot.name} ${userSnapshot.dateOfBirth}`
    userData.innerHTML += userInfo;
    })*/

    // Pobieranie wszystkich elementów z "folderu"

    const userRef = ref(database, "users");

    onValue(userRef, (snapshot) => {
        const userSnapshot = snapshot.val();
        const userKeys = Object.keys(userSnapshot);
        console.log(userKeys);
    })
}

// 5. Odczytywanie rekordów z Realtime
const loginUser = async () => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;
    try {
        // Logowanie
        const user = await signInWithEmailAndPassword(auth, valueEmail, valuePassword);
        const userUid = user.user.uid;
        getFromRealtime(userUid);
    } catch (error) {
        // Obsługa nieudanego logowania
        if (error.code === AuthErrorCodes.INVALID_PASSWORD) {
            errorLabel.innerHTML = "Podano nieprawidłowe hasło!";
        } else {
            errorLabel.innerHTML = error.code;
        }
    }
};

loginBtn.addEventListener('click', loginUser);
// 6. Aktualizowanie istniejących wpisów
update(ref(database, "users/EE9spsk6Upg49M7T34mAFuIIGQ73"), {
    name: "zmieniony name",
    dateOfBirth: "01-01-2020"
})

// 7. Usuwanie instniejących wpisów
remove(ref(database, "users/EE9spsk6Upg49M7T34mAFuIIGQ73"));
set(ref(database, "users/06h2oFTi1hOQsWulgdaMnwKZrP12"), null);

//pbranie idkow
const usersData = document.querySelector("#usersData")
const allUsersRef = ref(database, "users");
onValue(allUsersRef, (snapshot) => {
    const allUsers = snapshot.val();
    const allUsersUid = Object.keys(allUsers);
    console.log(allUsersUid);
    allUsersUid.forEach((userUid) => {
        getUserInfo(userUid);
    })

});

const getUserInfo = (uid) => {
    get(child(ref(database), "users/" + uid))
        .then((snapshot) => {
            console.log(snapshot.val());
            const userValues = snapshot.val();

            if (userValues.phone === undefined) {
                userValues.phone = "brak numeru telefonu";
            }
            const userDescription = `<p>${userValues.name} ma nr telefonu ${userValues.phone}</p>`
            usersData.innerHTML += userDescription
        })
        .catch((error) => {
            console.warn(error);
        })
}

//label o polaczeniu internetowym utraconym

const connectionStatusRef = ref(database, ".info/connected");

onValue(connectionStatusRef, (snapshot) => {
    console.log("connection: " +snapshot.val());
})
