import {
    initializeApp
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";

import {
    getStorage,
    ref as storageRef,
    uploadBytesResumable,
    getDownloadURL,
    listAll
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-storage.js";

import {
    getFirestore,
    doc,
    addDoc,
    getDoc,
    setDoc,
    query,
    collection,
    getDocs,
    where,
    updateDoc,
    deleteDoc
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";

import {
    getAuth,
    signInWithEmailAndPassword,
    AuthErrorCodes,
    createUserWithEmailAndPassword,
    signOut,
    onAuthStateChanged,
    GoogleAuthProvider
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";


import {
    getDatabase,
    set,
    ref,
    onValue,
    update,
    remove,
    get,
    child
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";


// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyA7jRfkQ3WeOu7PxOBDfyGVa2BWzN9meSU",
    authDomain: "project-arpfrontendpl1.firebaseapp.com",
    projectId: "project-arpfrontendpl1",
    storageBucket: "project-arpfrontendpl1.appspot.com",
    messagingSenderId: "1035264414402",
    appId: "1:1035264414402:web:17cebf490c83cb8f2ba15b",
    measurementId: "G-P4Q78T7H6G",
    databaseURL: "https://project-arpfrontendpl1-default-rtdb.europe-west1.firebasedatabase.app",
};



// 1. Inicjalizacja modułów
const app = initializeApp(firebaseConfig);
const storage = getStorage();
const auth = getAuth(app);

const db = getFirestore(app);
const database = getDatabase(); //realtime db
//const collectionNameUsers = "users";
const collectionNameItems = "items";

//2. mapowanie elementów UI

const selectImageInput = document.querySelector("#selectImageInput");
const uploadImageBtn = document.querySelector("#uploadImageBtn");
const uploadProgressLabel = document.querySelector("#uploadProgressLabel");
const itemName = document.querySelector("#itemName");
const itemPrice = document.querySelector("#itemPrice");

const emailForm = document.querySelector("#emailForm");
const passwordForm = document.querySelector("#passwordForm");
const nameForm = document.querySelector("#nameForm");
const dateOfBirthForm = document.querySelector("#dateOfBirthForm");
const loginBtn = document.querySelector("#loginBtn");
const signUpBtn = document.querySelector("#signUpBtn");
const userData = document.querySelector("#userData");

const itemsContainer = document.querySelector("#itemsContainer");

let user = {
    user: {
        uid: "nieprawidlowe_uid"
    }
};

// 3. Dodawanie elementów do bazy Realtime

const saveToRealtime = (uid) => {
    const valueEmail = emailForm.value;
    const valueName = nameForm.value;
    const valueDateOfBirth = dateOfBirthForm.value;
    console.log("valueDateOfBirth: " + valueDateOfBirth);
    set(ref(database, "users/" + uid), {
        email: valueEmail,
        name: valueName,
        dateOfBirth: valueDateOfBirth
    });
}
// 4. Dodanie możliwość rejestracji użytkownika
const signUpUser = async () => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;
    try {
        //const user = await createUserWithEmailAndPassword(auth, valueEmail, valuePassword);
        //console.log(user);
        // Dodanie danych do bazy danych (np. Firestore),
        // ale u nas w celach ćwieczniowych wykorzystamy Realtime Database
        const userUid = user.user.uid;
        saveToRealtime(userUid);
    } catch (error) {
        console.warn(error);
    }
};
signUpBtn.addEventListener('click', signUpUser);
const getFromRealtime = (uid) => {
    // Pobieranie jednego konkretnego elementu
    /*const userRef = ref(database, "users/" + uid);
    onValue(userRef, (snapshot) => {
    const userSnapshot = snapshot.val();
    let userInfo = `${userSnapshot.email} ${userSnapshot.name} ${userSnapshot.dateOfBirth}`
    userData.innerHTML += userInfo;
    })*/
    // Pobieranie wszystkich elementów z "folderu"
    const userRef = ref(database, "users");
    onValue(userRef, (snapshot) => {
        const userSnapshot = snapshot.val();
        const userKeys = Object.keys(userSnapshot);
        console.log(userKeys);
    })
}
// 5. Odczytywanie rekordów z Realtime
const loginUser = async () => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;
    try {
        // Logowanie
        user = await signInWithEmailAndPassword(auth, valueEmail, valuePassword);
        console.log(user);
        const userUid = user.user.uid;
        getFromRealtime(userUid);
    } catch (error) {
        // Obsługa nieudanego logowania
        if (error.code === AuthErrorCodes.INVALID_PASSWORD) {
            errorLabel.innerHTML = "Podano nieprawidłowe hasło!";
        } else {
            errorLabel.innerHTML = error.code;
        }
    }
};
loginBtn.addEventListener('click', loginUser);
// 6. Aktualizowanie istniejących wpisów
update(ref(database, "users/EE9spsk6Upg49M7T34mAFuIIGQ73"), {
    name: "zmieniony name",
    dateOfBirth: "01-01-2020"
})
// 7. Usuwanie instniejących wpisów
remove(ref(database, "users/EE9spsk6Upg49M7T34mAFuIIGQ73"));
set(ref(database, "users/06h2oFTi1hOQsWulgdaMnwKZrP12"), null);
// 7.1. Pobranie wszystkich IDków
const usersData = document.querySelector("#usersData");
const allUsersRef = ref(database, "users");
onValue(allUsersRef, (snapshot) => {
    const allUsers = snapshot.val();
    const allUsersUid = Object.keys(allUsers);
    allUsersUid.forEach((userUid) => {
        getUserInfo(userUid);
    });
});
// 7.2. Na podstawie pobranych IDków pobrać dane poszczególnych userów
// i wyświetlić dane w oczekiwanym formacie
const getUserInfo = (uid) => {
    get(child(ref(database), "users/" + uid))
        .then((snapshot) => {
            const userValues = snapshot.val();
            if (userValues.phoneNumber === undefined) {
                userValues.phoneNumber = "BRAK NUMERU TELEFONU";
            }
            const userDescription = `<p>${userValues.name} ma numer telefonu: ${userValues.phoneNumber}</p>`;
            usersData.innerHTML += userDescription;
        })
        .catch((error) => {
            console.warn(error);
        })
}

//wybieranie nazwy pliku z dysku
let imageName = "";
let file = undefined;

selectImageInput.onchange = event => {
    file = event.target.files[0];
    imageName = file.name;

    console.log(imageName);
    //jak zabezpieczyć przed formatem  a.b.c.jpg ? => regex (wyrażenia regularne)  
};

//pobierzmay wszystkie pliki z folderu starageu

const storageImagesRef = storageRef(storage, "item-images");
const itemsRef = collection(db, collectionNameItems);
const itemsList = [];

//dodawanie nowego przedmiotu
uploadImageBtn.onclick = async () => {
    console.log("upload btn clicked");
    const metaData = {
        contentType: file.type
    };
    //stworzenie wskażnika do nowego obrazka
    const storageReff = storageRef(storage, `item-images/${imageName}`);
    console.log("image name1: ", imageName);
    console.log("item name: ", itemName.value);
    console.log("item price: ", itemPrice.value);

    //rozpoczeniciedr procesue wrzucania pliku do firebase storage
    const uploadTask = uploadBytesResumable(storageReff, file, metaData);

    uploadTask.on('state-changed', (snapshot) => {
        console.log(snapshot);
        const progress = `${snapshot.bytesTransferred / snapshot.totalBytes * 100}%`;
        uploadProgressLabel.innerText = progress;
    },
        (error) => {
            console.warn(error);
        },
        () => {
            console.log("then...");
            getDownloadURL(uploadTask.snapshot.ref)
                .then((url) => {
                    //display added image
                    uploadedImage.setAttribute("src", url);

                    //add new item into firebase
                    console.log("image name2:", imageName);



                    const item = {
                        name: itemName.value,
                        price: itemPrice.value,
                        image: url
                    }

                    addDoc(itemsRef, item)
                        .then((addedDoc) => {
                            console.log("Document added with id: ", addedDoc.id);
                            //console.log(addedDoc);
                            //displayItem(addedDoc);
                        })
                });
        });
};


//show available items
const displayItems = async () => {
    const itemsSnap = await getDocs(itemsRef)
    itemsSnap.forEach((doc) => {
        const item = document.createElement("div");
        item.classList.add("item");

        console.log(doc.data().id);

        const name = document.createElement("h3");
        name.innerHTML = `Name: ${doc.data().name}`;
        item.appendChild(name);

        const price = document.createElement("p")
        price.innerHTML = `Price: ${doc.data().price}`;
        item.appendChild(price);

        const image = document.createElement("img")
        image.src = doc.data().image;
        item.appendChild(image);

        const buttonContainer = document.createElement("div");
        const deleteButton = document.createElement("button");
        deleteButton.textContent = "Delete"
        buttonContainer.appendChild(deleteButton);
        item.appendChild(buttonContainer);
        
        itemsContainer.appendChild(item);
    })
}

displayItems();

const displayItem = (doc) => {

    const item = document.createElement("div");

    const name = document.createElement("h3");

    console.log(doc.name);

    name.innerHTML = `Name: ${doc.name}`;
    item.appendChild(name);

    const price = document.createElement("p")
    price.innerHTML = `Price: ${doc.price}`;
    item.appendChild(price);

    const image = document.createElement("img")
    image.src = doc.image;
    item.appendChild(image);

    //const deleteButton = "<button onclick=deleteBook(" + bookId + ")>Delete</button>";
    //book.appendChild(deleteButton);
    itemsContainer.appendChild(item);

}

//pobieranie url konkretnego pliku
/*
const displayItems = () => {
    itemsList.forEach((imagePath) => {
        getDownloadURL(ref(storage, imagePath))
            .then((url) => {
                const imgElement = document.createElement("img");
                imgElement.src = url;
                //imgElement.width = "300 px";
                imgElement.width = 250;
                imgElement.height = 250;
                document.body.appendChild(imgElement);
                console.log(url);
            })
            .catch((error) => {
                console.warn("moj errror");
                console.warn(error);
            })
    });
}

*/

/*
listAll(storageImagesRef)
    .then((listOfImages) => {

        listOfImages.items.forEach((image) => {
            console.log(image._location.path.substring(7));
            imagesList.push(image._location.path);
        })
        //console.log(listOfImages);
        displayImages();

    })
    .catch(error => {
        console.warn("nasz errror");
        console.warn(error);
    })

console.log(imagesList)
*/