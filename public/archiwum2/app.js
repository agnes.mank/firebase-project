import {
    initializeApp
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";


import {
    getFirestore,
    doc,
    getDoc,
    setDoc,
    query,
    collection,
    getDocs,
    where,
    updateDoc,
    deleteDoc
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";

import {
    getAuth,
    signInWithEmailAndPassword,
    AuthErrorCodes,
    createUserWithEmailAndPassword,
    signOut,
    onAuthStateChanged,
    GoogleAuthProvider
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";


// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyA7jRfkQ3WeOu7PxOBDfyGVa2BWzN9meSU",
    authDomain: "project-arpfrontendpl1.firebaseapp.com",
    projectId: "project-arpfrontendpl1",
    storageBucket: "project-arpfrontendpl1.appspot.com",
    messagingSenderId: "1035264414402",
    appId: "1:1035264414402:web:17cebf490c83cb8f2ba15b",
    measurementId: "G-P4Q78T7H6G"
  };
  
// Initialize Firebase
const app = initializeApp(firebaseConfig);

//podlaczenie modulu
const auth = getAuth(app);

//podlaczenie modulu
const db = getFirestore(app);

//definiowanie elementow UI
const emailForm = document.querySelector("#emailForm");
const passwordForm = document.querySelector("#passwordForm");
const loginBtn = document.querySelector("#loginBtn");
const errorLabel = document.querySelector("#errorLabel");
const signUpBtn = document.querySelector("#signUpBtn");
const authenticatedUser = document.querySelector("#authenticatedUser");
const logOutBtn = document.querySelector("#signOutBtn");
const loginGoogleBtn = document.querySelector("#loginGoogleBtn")

// zebranie ifnormacji o dokumencie do pobrania
const collectionName = "books";

//dodanie logowania
const loginUser = async() => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;

    try {
        // Logowanie
        const user = await signInWithEmailAndPassword(auth, valueEmail, valuePassword);
        console.log(user);
    } catch(error) {
        // Obsługa nieudanego logowania
        if (error.code === AuthErrorCodes.INVALID_PASSWORD) {
            errorLabel.innerHTML = "Podano nieprawidłowe hasło!";
        } else {
            errorLabel.innerHTML = error.code;
        }
    }
};

loginBtn.addEventListener('click', loginUser);

//dodawanie rejestracji

const signUpUser = async() => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;
    
    try {
        await createUserWithEmailAndPassword(auth, valueEmail, valuePassword);
        console.log(valueEmail, valuePassword)

    } catch (error) {
        console.warn(error);
    }
};

signUpBtn.addEventListener('click', signUpUser);

// dodanie możliwości wylogowania
const logoutUser = async () => {
    await signOut (auth);
    console.log("user logged out");
}

logOutBtn.addEventListener('click', logoutUser);

//nasluchiwanie zmiany statusu sesji usera

const authUserObserver = async () => {
    onAuthStateChanged(auth, user => {
        if (user){
            loginForm.style.display = 'none';
            authenticatedUser.style.display = 'block';
        } else {
            loginForm.style.display = 'block';
            authenticatedUser.style.display = 'none';
        }
    });
};

authUserObserver()


