import {
    initializeApp
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";


import {
    getFirestore,
    doc,
    getDoc,
    setDoc,
    query,
    collection,
    getDocs,
    where,
    updateDoc,
    deleteDoc
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";


// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyA7jRfkQ3WeOu7PxOBDfyGVa2BWzN9meSU",
    authDomain: "project-arpfrontendpl1.firebaseapp.com",
    projectId: "project-arpfrontendpl1",
    storageBucket: "project-arpfrontendpl1.appspot.com",
    messagingSenderId: "1035264414402",
    appId: "1:1035264414402:web:17cebf490c83cb8f2ba15b",
    measurementId: "G-P4Q78T7H6G"
  };
  
// Initialize Firebase
const app = initializeApp(firebaseConfig);

//podlaczenie modulu
const db = getFirestore(app);


// zebranie ifnormacji o dokumencie do pobrania
const collectionName = "books";
const documentId = "1";

//pobranie info o konkretnej ksiazce
const docRef = doc(db, collectionName,documentId);
const docSnap = await getDoc(docRef);

console.log("doc snap:", docSnap.data());

///if (docSnap.exists()){
//    console.log
//}

//dodawanie nowej ksiazki
const bookRef = doc(db, collectionName, "2")
const book = {
    Title: "Quo Vadis",
    Author: "Sienkiewicz",
    Pages: 450,
    Availability: true
}

setDoc (bookRef,book);

const c = collection(db, collectionName);
const q = query(c, where("Availability", "==" , true) );
const querySnap = await getDocs(q);

querySnap.forEach((doc) => {
    console.log("query (available):", doc.data());
});

// 6. update
const bookForUpdateRef = doc(db, collectionName,"2")
const bookForUpdate = {
    Availability: false
};

await updateDoc(bookForUpdateRef, bookForUpdate);
//console.log("updated:" , doc.data)

const querySnap2 = await getDocs(c);
querySnap2.forEach((doc) => {
    console.log("updated:", doc.data());
});

//7. delete
//await deleteDoc(doc(db, collectionName, "2"));

const querySnap3 = await getDocs(c);
querySnap3.forEach((doc) => {
    console.log("after deletion:", doc.data());
});

const container = document.getElementById('container')

querySnap3.forEach((doc) => {
    console.log("books:", doc.data());

    const book = document.createElement("div");

    const title = document.createElement("h2");
    title.innerHTML = `Title: ${doc.data().Title}`;
    book.appendChild(title);

    const author = document.createElement("p")
    author.innerHTML = `Author: ${doc.data().Author}`;
    book.appendChild(author);

    const pages = document.createElement("p")
    pages.innerHTML = `Pages: ${doc.data().Pages}`;
    book.appendChild(pages);

    const available = document.createElement("p")
    if (doc.data().Availability) {
        available.innerHTML = `Availability: In Stock`
    }
    else{
        available.innerHTML = `Availability: Out of Stock`
    }

    
    book.appendChild(available);


    const bookId = doc.data().id;
    //const deleteButton = "<button onclick=deleteBook(" + bookId + ")>Delete</button>";
    //book.appendChild(deleteButton);
    container.appendChild(book);


});

// 9**. Dodanie możliwości usuwanie rekordu z poziomu UI

const booksListDiv = document.querySelector("#booksList");

const deleteBook = async(bookId) => {
    const id = bookId.target.dataset.id;
    deleteDoc(doc(db, collectionName, id));

    booksListDiv.innerHTML = "";
    initBooksList();
};

const initBooksList = async() => {
    const queryAllBooks = query(collection(db, collectionName));
    const allBooks = await getDocs(queryAllBooks);

    let booksList = "<ul>";
    allBooks.forEach((book) => {
        const bookTitle = book.data().title;
        const bookId = book.id;

        booksList += `<li> ${bookTitle}`;
        booksList += `<button type="button" data-id="${bookId}" class='btnDel'>Delete</button>`
        booksList += `</li>`;
    });
    booksList += `</ul>`;

    booksListDiv.innerHTML = booksList;

    const deleteBtns = document.querySelectorAll(".btnDel");
    deleteBtns.forEach((singleDeleteBtn) => {
        singleDeleteBtn.addEventListener("click", deleteBook);
    })
};

initBooksList();