import {
    initializeApp
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";

import {
    getStorage,
    ref,
    uploadBytesResumable,
    getDownloadURL,
    listAll
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-storage.js";

import {
    getDatabase,
    set,
    //ref,
    onValue,
    update,
    remove,
    get,
    child
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

import {
    getFirestore,
    doc,
    getDoc,
    setDoc,
    query,
    collection,
    getDocs,
    where,
    updateDoc,
    deleteDoc
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";


// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyA7jRfkQ3WeOu7PxOBDfyGVa2BWzN9meSU",
    authDomain: "project-arpfrontendpl1.firebaseapp.com",
    projectId: "project-arpfrontendpl1",
    storageBucket: "project-arpfrontendpl1.appspot.com",
    messagingSenderId: "1035264414402",
    appId: "1:1035264414402:web:17cebf490c83cb8f2ba15b",
    measurementId: "G-P4Q78T7H6G",
    databaseURL: "https://project-arpfrontendpl1-default-rtdb.europe-west1.firebasedatabase.app",
};



// 1. Inicjalizacja modułów
const app = initializeApp(firebaseConfig);
const storage = getStorage();
const database = getDatabase();


const db = getFirestore(app);
const collectionName = "users";


//2. mapowanie elementów UI

const imageNameInput = document.querySelector("#imageNameInput");
const selectImageInput = document.querySelector("#selectImageInput");
const uploadImageBtn = document.querySelector("#uploadImageBtn");
const uploadProgressLabel = document.querySelector("#uploadProgressLabel");
const uploadedImage = document.querySelector("#uploadedImage");

// 3. wrzucanie lokalnych plkikow

//wybieranie nazwy pliku z dysku
let imageName = "";
let file = undefined;

selectImageInput.onchange = event => {
    file = event.target.files[0];
    imageName = file.name;

    //console.log(imageProperties);
        //jak zabezpieczyć przed formatem  a.b.c.jpg ? => regex (wyrażenia regularne)  
};

uploadImageBtn.onclick = async() => {
    const metaData = {
        contentType: file.type
    };
    //stworzenie wskażnika do nowego obrazka
    const storageRef = ref(storage, `images/${imageName}`);
    console.log("image name1: ", imageName);

    //rozpoczeniciedr procesue wrzucania pliku do firebase storage
    const uploadTask = uploadBytesResumable(storageRef, file, metaData);

    uploadTask.on('state-changed', (snapshot) => {
        //console.log(snapshot);
        const progress = `${snapshot.bytesTransferred/snapshot.totalBytes *100}%`;
        uploadProgressLabel.innerText = progress;

    },
    (error) => {
        console.warn(error);
    },
    () => {
        getDownloadURL(uploadTask.snapshot.ref)
            .then((url) => {
                uploadedImage.setAttribute("src", url);
                  //proba dodania do userow w firebase
                console.log("image name2:" , imageName);
                const userRef = doc(db, collectionName, imageName)
                const user = {
                    username: imageName,
                    imageUrl: url
                }
                setDoc(userRef,user);
            })
    });
};


//pobierzmay wszystkie pli z folderu starageu

const storageImagesRef = ref(storage, "images");
const imagesList = [];

//let imagesDisplayList = "";
const generateDropdown = () => {
    const selectTag = document.createElement("select");
    selectTag.id = "selectImage";
    document.body.appendChild(selectTag);
    imagesList.forEach((image) => {
        const optionImage = document.createElement("option");
        optionImage.value = image;
        optionImage.text = image.substring(7);
        selectTag.appendChild(optionImage);
    })
}

const generateDeleteBtn = () => {
    const button = document.createElement("button");
    button.id = "deleteImageBtn";
    //zrobic napis na przyciksu
    document.body.appendChild(button);
    return button;
}


const deleteImageFromStorage = (imagePath) => {
    const imageRef = ref (storage, imagePath );

    deleteObject(imageRef)
        .then(()=> {
            console.log(`'Poprawnie usunieto plik' ${imagePath}`)
        })
}



//pobieranie url konkretnego pliku

const displayImages = () => {
    imagesList.forEach((imagePath) => {
        getDownloadURL(ref(storage, imagePath))
            .then((url) => {
                const imgElement = document.createElement("img");
                imgElement.src = url;
                //imgElement.width = "300 px";
                imgElement.width = 250;
                imgElement.height = 250;
                document.body.appendChild(imgElement);
                console.log(url);
            })
            .catch((error) => {
                console.warn("moj errror");
                console.warn(error);
            })
    });
}

listAll(storageImagesRef)
    .then((listOfImages) => {
        
        listOfImages.items.forEach((image) =>{
            console.log(image._location.path.substring(7));
            imagesList.push(image._location.path);
        })
        //console.log(listOfImages);
        displayImages();
        generateDropdown();
        generateDeleteBtn().onclick = () => {
            //
            const selectImage = document.querySelector("#selectImage");
            deleteImageFromStorage(); //?
        }

    })
    .catch(error => {
        console.warn("nasz errror");
        console.warn(error);
    })

console.log(imagesList)

//delete objectr
/*
deleteObject(imageToDeleteRef)
    .then((res) => {
        console.log(res);
    })

*/


